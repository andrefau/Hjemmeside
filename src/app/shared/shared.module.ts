import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DxTextBoxModule,
		DxButtonModule,
		DxValidatorModule } from 'devextreme-angular';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	exports: [
		DxTextBoxModule,
		DxButtonModule,
		DxValidatorModule
	]
})
export class SharedModule { }
