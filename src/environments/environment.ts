// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
	production: false,
	firebase: {
		apiKey: 'AIzaSyBLGQ6IW0qwvUeBzsDneLQuelf5RfnMuqA',
		authDomain: 'hjemmeside-5050.firebaseapp.com',
		databaseURL: 'https://hjemmeside-5050.firebaseio.com',
		projectId: 'hjemmeside-5050',
		storageBucket: 'hjemmeside-5050.appspot.com',
		messagingSenderId: '262325347358'
	}
};
